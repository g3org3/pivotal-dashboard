/*
 * Routes
 */

module.exports = {
	'/': 'MainController.index',
	'/data' : 'MainController.data',
	'/data2': 'MainController.data2',
	'/projects': 'MainController.projects',
	'GET /token': 'MainController.token',
	'POST /token': 'MainController.utoken'
}