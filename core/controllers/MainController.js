
var needle = require('needle');
var _ = require('lodash')
global['_token'] = "29812bc577811aaa980f3eee8b366c52"

module.exports = {
	index: function(req, res) {
		res.render('index', {title: "Webjs"});
	},
	utoken: function(req, res){
		var token = req.param('token');
		_token = token;
		res.json('ok')
	},
	token: function(req, res){
		res.json(_token);
	},
	data2: function(req, res){
		var id = req.param('id');
		var pid = req.param('pid')// || '1274132';
		

		var opts = {
			headers: {
				"X-TrackerToken": _token
			}
		}

		var url = 'https://www.pivotaltracker.com/services/v5/projects/'+pid+'/iterations?'
		if(id==undefined)
			url += 'scope=current'
		else
			url += 'limit=1&offset='+ (parseInt(id)-1)
			
			
		needle.get(url, opts, function(err, response){
			if(response.body[0]){
			var respuesta = parse(response.body[0])
			res.json(respuesta);
			} else {
				console.log("NO SE PUDO", _token, pid)
				require('purdy')(response.body)
				res.json(response.body[0]);
			}
		})
	},
	projects: function(req, res){
		var opts = {
			headers: {
				"X-TrackerToken": _token
				//"b2590d3a2b02a2b3efccebf740438850"
			}
		}

		var url = 'https://www.pivotaltracker.com/services/v5/projects/';
			
		needle.get(url, opts, function(err, response){
			res.json(response.body)
		})
	}
}
function parse(info){
	var points = {};
	var total = _.reduce(info.stories, function(total, story){
		if(story.story_type!="release")
			if(story.estimate)
				total += story.estimate;
		return total;
	}, 0)

	var done = total;

	var stories = _.filter(info.stories, function(story){
		return story.story_type!="release"
	})
	
	// other
	var current_date = info.start
	var fecha = new Date(current_date);
	for(var i=0; i<14; i++){
		points[fecha.toISOString().substring(0, fecha.toISOString().indexOf("T"))] = {
			y: 0,
			x: i
		};
		fecha.setDate(fecha.getDate() + 1)
	}
	
	_.chain(stories)
	.filter(function(story){
		return (story.story_type!="release" && story.estimate && story.accepted_at)
	})
	.map(function(story){
		var key = story.accepted_at.substring(0, story.accepted_at.indexOf("T"))
		var s = {
			date: key,
			estimate: story.estimate
		}
		points[key].y += s.estimate
		return s;
	}).value()
	
	points = _.reduce(points, function(accum, point, key){
		done -= point.y;
		point.y = done;
		accum.push(point)
		return accum;
	}, [])
	info.points = points;
	info.total = total;
	info.done = total - done;
	return info;
}