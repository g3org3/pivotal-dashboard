/**
 * MVC LOGIC
 */
angular.module('app', [])
.controller('ctrl', function($scope, $http){
	
	// if(!localStorage.state && !localStorage.id) {
		$scope.currenttab = 0;
		// window.history.pushState('listview', '', '/list');
	// } else {
		// $scope.currenttab = parseInt(localStorage.id);
		// window.history.pushState(localStorage.state, '', '/'+localStorage.state);
	// }
	// window.onpopstate = function(e) { 
	// 	switch(e.state){
	// 		case 'list':
	// 			$scope.currenttab=0;
	// 			break;
	// 		case 'chart':
	// 			$scope.currenttab=1;
	// 			break;
	// 		case 'settings':
	// 			$scope.currenttab=2;
	// 			break;
	// 	}
	// 	$scope.$apply();
	// }

	$scope.showinfo = function(story){
		(document.getElementsByClassName("sweet-alert")[0]).style.width="700px";
		(document.getElementsByClassName("sweet-alert")[0]).style.marginLeft="-350px";
		swal({
			title: story.name,
			text: "<pre style='text-align:left;'>"+(story.description||"-")+"</pre>",
			html: true
		});
	}
	
	$scope.set = function(id){
		$scope.currenttab=id;
		if(id==0){
			// window.history.pushState('list', 'List', '/list');
			localStorage.id = id;
			localStorage.state = 'list'
		}
		else if(id==1){
			// window.history.pushState('chart', 'Chart', '/chart');
			localStorage.id = id;
			localStorage.state = 'chart'
		}
		else if(id==2){
			// window.history.pushState('settings', 'settings', '/settings');
			localStorage.id = id;
			localStorage.state = 'settings'
		}
	}

	$http.get('/projects').then(function(res){
		$scope.projects = res.data;
		$scope.selectProject = res.data[0];
		$scope.refresh();
	})

	$http.get('/token').then(function(res){
		$scope.token = res.data;
	})

	$scope.updateToken = function(){
		$http.post('/token', {token: $scope.token}).then(function(r){
			$http.get('/projects').then(function(res){
				$scope.projects = res.data;
				$scope.selectProject = res.data[0];
			})
		})
	}

	$scope.refresh = function(id){
		// console.log($scope.selectProject)
		var url = "/data2"
		if(id!=undefined) {
			$scope.set(1);
		}
			if(document.getElementsByClassName("sweet-alert")[0]){
				(document.getElementsByClassName("sweet-alert")[0]).style.width="478px";
				(document.getElementsByClassName("sweet-alert")[0]).style.marginLeft="-256px";
			}
			swal({
				type: "success",
				title: "Actualizado!",
				timer: 1500,
				showConfirmButton: false
			});
		//}
		$http.post(url, {id: id, pid: $scope.selectProject.id}).then(function(res){
			$scope.info = res.data;

			$scope.stories = _.filter($scope.info.stories, function(story){
				return story.story_type!="release"
			})

			$(function(){
				initChart(res.data.points, res.data.total, id!=undefined);
			});
		})
	}

	
})